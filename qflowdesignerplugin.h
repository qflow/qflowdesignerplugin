#ifndef QFLOWDESIGNERPLUGIN_H
#define QFLOWDESIGNERPLUGIN_H

#include <iwidgetplugin.h>

class QFlowDesignerPlugin : public QObject, QmlDesigner::IWidgetPlugin
{
    Q_OBJECT
    #if QT_VERSION >= 0x050000
        Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QmlDesignerPlugin" FILE "qflowdesignerplugin.json")
    #endif
        Q_DISABLE_COPY(QFlowDesignerPlugin)
        Q_INTERFACES(QmlDesigner::IWidgetPlugin)
public:
    QFlowDesignerPlugin();
    ~QFlowDesignerPlugin(){}

    QString metaInfo() const;
    QString pluginName() const;
};

#endif // QFLOWDESIGNERPLUGIN_H
