#-------------------------------------------------
#
# Project created by QtCreator 2013-05-24T07:59:44
#
#-------------------------------------------------

TARGET = QFlowDesignerPlugin
TEMPLATE = lib
CONFIG += plugin
DESTDIR = /home/michal/workspace/qt-creator/lib/qtcreator/qmldesigner

QTCREATOR_SOURCES = $$(QTC_SOURCE)
isEmpty(QTCREATOR_SOURCES):QTCREATOR_SOURCES=/home/michal/workspace/qt-creator

SOURCES += qflowdesignerplugin.cpp

HEADERS += qflowdesignerplugin.h

include ($$QTCREATOR_SOURCES/src/plugins/qmldesigner/designercore/iwidgetplugin.pri)

OTHER_FILES += \
    qflowdesignerplugin.json \
    qflow.metainfo

RESOURCES += \
    resources.qrc

